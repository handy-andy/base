import pymysql


class MySQL:
    def __init__(self, user="root", password="password", host="127.0.0.1", database=""):
        self.cnx = None
        self.user = user
        self.password = password
        self.host = host
        self.database = database
        self.ready = False
        self.connect()
        self._cnx = None

    @property
    def cnx(self):
        if not getattr(self._cnx, "open", None):
            self.connect()
        return self._cnx

    @cnx.setter
    def cnx(self, value):
        self._cnx = value

    def connect(self):
        try:
            self._cnx = pymysql.connect(host=self.host,
                                        user=self.user,
                                        password=self.password,
                                        db=self.database,
                                        charset='utf8mb4',
                                        cursorclass=pymysql.cursors.DictCursor)
        except:
            raise

    def update(self, table, where=None, values=None):
        if where is None:
            where = {}
        if values is None:
            values = {}
        try:
            with self.cnx.cursor() as cursor:
                inserter = "UPDATE {0} SET {1} WHERE {2}".format(table, ", ".join([f"{k} = %({k})s" for k in values]), " AND ".join([f"{k} = %({k})s" for k in where]))
                cursor.execute(inserter, {**values, **where})
                self.cnx.commit()
        except:
            raise

    def insert(self, table, values):
        try:
            with self.cnx.cursor() as cursor:
                keys = values.keys()
                inserter = "INSERT INTO {0} ({1}) VALUES (%({2})s)".format(table, ", ".join(keys), ")s, %(".join(keys))
                cursor.execute(inserter, values)
                self.cnx.commit()
        except:
            raise

    def select(self, table, values, where=None):
        fetched = []
        try:
            with self.cnx.cursor() as cursor:
                selector = "SELECT {0} FROM {1}".format(", ".join(values), table)
                if isinstance(where, dict):
                    where_query = " WHERE {0}".format(" AND ".join([f"{k} = %({k})s" for k in where.keys()]))
                    cursor.execute(selector + where_query, where)
                else:
                    cursor.execute(selector)
                # items = []
                fetched = cursor.fetchall()
        except:
            raise
        finally:
            return fetched

    def exists(self, table, where=None):
        rc = 0
        try:
            with self.cnx.cursor() as cursor:
                selector = "SELECT * FROM {0} WHERE {1}".format(table, " AND ".join(["{0} = %({0})s".format(key) for key in where]))
                cursor.execute(selector, where)
                cursor.fetchall()
                rc = cursor.rowcount
        except:
            raise
        finally:
            return rc > 0

    def delete(self, table, where=None):
        try:
            with self.cnx.cursor() as cursor:
                selector = "DELETE FROM {0} WHERE {1}".format(table, " AND ".join(["{0} = %({0})s".format(key) for key in where]))
                cursor.execute(selector, where)
                self.cnx.commit()
        except:
            raise
