from . import methods


def setup(settings):
    if not hasattr(methods, settings.method):
        raise Exception()

    method_callback = getattr(methods, settings.method)
    return method_callback(**settings.kwargs)
