import datetime


class ListImitator(list):
    def __init__(self, initial, on_update):
        super(ListImitator, self).__init__(initial)
        self.__on_update = on_update

    def append(self, value):
        self.__on_update(value, "append")
        super(ListImitator, self).append(value)

    def remove(self, value):
        self.__on_update(value, "remove")
        super(ListImitator, self).remove(value)

    def get_index(self, index, default=None):
        try:
            return super(ListImitator, self).__getitem__(index)
        except IndexError:
            return default

    def get(self, item, default=None):
        try:
            for v in self:
                if all([v[k] == item[k] for k, t in item.items()]):
                    return v
        except IndexError:
            return default


class ReadOnlyDict(dict):
    def __readonly__(self, *args, **kwargs):
        raise RuntimeError("Cannot modify read-only object!")
    __setitem__ = __readonly__
    __delitem__ = __readonly__
    pop = __readonly__
    popitem = __readonly__
    clear = __readonly__
    update = __readonly__
    setdefault = __readonly__
    del __readonly__


class Config:
    def __init__(self, db, guild_id):
        self.__guild_id = guild_id
        self.__db = db

        cmds_cogs_disabled = self.__db.select(
            "dino_server_cogs_cmds_disabled",
            ["server_disabled_type", "server_disabled_name"],
            where={"server_id": self.__guild_id}
        )
        cmds_disabled = list(filter(lambda i: i["server_disabled_type"] == "cmd", cmds_cogs_disabled))
        cogs_disabled = list(filter(lambda i: i["server_disabled_type"] == "cog", cmds_cogs_disabled))

        chans_restricted = self.__db.select(
            "dino_server_channels_restricted",
            ["server_channel_id"],
            where={"server_id": self.__guild_id}
        )
        muted = self.__db.select("dino_server_muted", ["muted_id", "muted_time"], where={"server_id": self.__guild_id})

        self.cmds_disabled = ListImitator(
            [
                item["server_disabled_name"] for item in cmds_disabled
            ] if len(cmds_disabled) > 0 else [],
            self.cmds_disabled_on_update
        )
        self.cogs_disabled = ListImitator(
            [
                item["server_disabled_name"] for item in cogs_disabled
            ] if len(cogs_disabled) > 0 else [],
            self.cogs_disabled_on_update
        )
        self.channels_restricted = ListImitator(
            [
                int(item["server_channel_id"]) for item in chans_restricted
            ] if len(chans_restricted) > 0 else [],
            self.channels_restricted_on_update
        )
        self.muted = ListImitator(
            [
                ReadOnlyDict(item) for item in muted
            ] if len(muted) > 0 else [],
            self.muted_on_update
        )

        # cleanup
        del cmds_cogs_disabled
        del cmds_disabled
        del cogs_disabled
        del chans_restricted
        del muted

        self.__prefix = self.get_conf("prefix", str, ",")
        self.__suggest_channel = self.get_conf("suggest_channel", int, 0)
        self.__suggest_enabled = self.get_conf("suggest_enabled", int, 0)
        self.__auto_role_id = self.get_conf("auto_role_id", int, 0)
        self.__auto_role_enabled = self.get_conf("auto_role_enabled", int, 0)
        self.__auto_welcome_enabled = self.get_conf("auto_welcome_enabled", int, 0)
        self.__auto_welcome_image_enabled = self.get_conf("auto_welcome_image_enabled", int, 0)
        self.__auto_welcome_message = self.get_conf("auto_welcome_message", str, "")
        self.__auto_welcome_channel = self.get_conf("auto_welcome_channel",  int, 0)

        self.no_mute = False

    def get_conf(self, key, modifier, default):
        item = self.__db.select("dino_server_config", ["server_config_value"],
                                where={"server_id": self.__guild_id, "server_config_key": key})
        return modifier(item[0]["server_config_value"]) if len(item) > 0 else default

    def set_conf(self, key, value):
        where = {"server_id": self.__guild_id, "server_config_key": key}
        if self.__db.exists("dino_server_config", where):
            self.__db.update("dino_server_config", where, {"server_config_value": value})
        else:
            self.__db.insert("dino_server_config", {**where, "server_config_value": value})

    def muted_on_update(self, value, action):
        action = self.__db.delete if action == "remove" else self.__db.insert
        action("dino_server_muted", {"server_id": self.__guild_id, "muted_id": value["muted_id"],
                                     "muted_time": value["muted_time"]})

    def channels_restricted_on_update(self, value, action):
        if action == "remove":
            self.__db.delete(
                "dino_server_channels_restricted",
                where={"server_id": self.__guild_id, "server_channel_id": value}
            )
        elif action == "append":
            self.__db.insert(
                "dino_server_channels_restricted",
                 {
                     "server_id": self.__guild_id,
                     "server_channel_id": value,
                 }
             )

    def cogs_disabled_on_update(self, value, action):
        action = self.__db.delete if action == "remove" else self.__db.insert
        action("dino_server_cogs_cmds_disabled",
               {"server_id": self.__guild_id, "server_disabled_type": "cog", "server_disabled_name": value})

    def cmds_disabled_on_update(self, value, action):
        action = self.__db.delete if action == "remove" else self.__db.insert
        action("dino_server_cogs_cmds_disabled",
               {"server_id": self.__guild_id, "server_disabled_type": "cmd", "server_disabled_name": value})

    @property
    def prefix(self):
        return self.__prefix

    @prefix.setter
    def prefix(self, value):
        self.set_conf("prefix", value)
        self.__prefix = value

    @property
    def suggest_channel(self):
        return self.__suggest_channel

    @suggest_channel.setter
    def suggest_channel(self, value):
        self.set_conf("suggest_channel", value)
        self.__suggest_channel = value

    @property
    def suggest_enabled(self):
        return self.__suggest_enabled

    @suggest_enabled.setter
    def suggest_enabled(self, value):
        self.set_conf("suggest_enabled", value)
        self.__suggest_enabled = value

    @property
    def auto_role_id(self):
        return self.__auto_role_id

    @auto_role_id.setter
    def auto_role_id(self, value):
        self.set_conf("auto_role_id", value)
        self.__auto_role_id = value

    @property
    def auto_role_enabled(self):
        return self.__auto_role_enabled

    @auto_role_enabled.setter
    def auto_role_enabled(self, value):
        self.set_conf("auto_role_enabled", value)
        self.__auto_role_enabled = value

    @property
    def auto_welcome_message(self):
        return self.__auto_welcome_message

    @auto_welcome_message.setter
    def auto_welcome_message(self, value):
        self.set_conf("auto_welcome_message", value)
        self.__auto_welcome_message = value

    @property
    def auto_welcome_enabled(self):
        return self.__auto_welcome_enabled

    @auto_welcome_enabled.setter
    def auto_welcome_enabled(self, value):
        self.set_conf("auto_welcome_enabled", value)
        self.__auto_welcome_enabled = value

    @property
    def auto_welcome_image_enabled(self):
        return self.__auto_welcome_image_enabled

    @auto_welcome_image_enabled.setter
    def auto_welcome_image_enabled(self, value):
        self.set_conf("auto_welcome_image_enabled", value)
        self.__auto_welcome_image_enabled = value

    @property
    def auto_welcome_channel(self):
        return self.__auto_welcome_channel

    @auto_welcome_channel.setter
    def auto_welcome_channel(self, value):
        self.set_conf("auto_welcome_channel", value)
        self.__auto_welcome_channel = value
