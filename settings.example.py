from types import SimpleNamespace

settings = {
    "core": SimpleNamespace(
        allow_custom_prefix=True,
        default_prefix=",",

        bot_token="bot_token",
        bot_log_channel=508750393921110026,
        bot_owner=[
            232191905394327562,
            218415631479996417
        ]
    ),

    "database": SimpleNamespace(
        method="MySQL",
        kwargs={
            "user": "mysql_username",
            "password": "mysql_password",
            "host": "mysql_hostname",
            "database": "mysql_database_name"
        }

        # method = "ArangoDB",
        #  kwargs = {
        #     "username": "root",
        #     "password": "pass11"
        # }
    ),

    "cogs": {
        "reddit": SimpleNamespace(
            client_id = "1234abcd",
            client_secret = "abcd!1234?",
            username = "reddit_user",
            password = "reddit_user_pass"
        ),

        "osu": SimpleNamespace(
            token="your_osu_token",
            emoji={
                "A": "<:OsuA:467975976983461889>",
                "B": "<:OsuB:467975977650356224>",
                "C": "<:OsuC:467975977511813121>",
                "D": "<:OsuD:467975978119856139>",
                "S": "<:OsuS:467975978560258049>",
                "SH": "<:OsuSH:467975978443079680>",
                "X": "<:OsuXH:467975979512365066>",
                "XH": "<:OsuXH:467975979126489088>",
                "SS": "<:OsuX:467975979512365066>",
                "SSH": "<:OsuXH:467975979126489088>"
            }
        )
    }
}
