import discord
import sys
import time
import traceback
import db
import importlib

from discord.ext import commands
from helpformatter import Formatter
from os import listdir
from os.path import isfile, join
from config import Config
from settings import settings
from inspect import signature


class Bot(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix=Bot.prefix_handler, formatter=Formatter())

        self.ready = False
        self.servopts = {}

        if not hasattr(self, 'uptime'):
            self.uptime = time.time()

        self.db = db.setup(settings["database"])

    # per-server prefix handler
    @staticmethod
    def prefix_handler(self, message):
        if self.ready and settings["core"].allow_custom_prefix and message.guild:
            prefix = self.servopts[message.guild.id].prefix
        else:
            prefix = settings["core"].default_prefix

        return commands.when_mentioned_or(prefix)(self, message)

    def load_extension(self, name):
        if name in self.extensions:
            return

        lib = importlib.import_module(name)
        if not hasattr(lib, 'setup'):
            del lib
            del sys.modules[name]
            raise discord.ClientException('extension does not have a setup function')

        ext = name.split('.')[-1]

        if len(signature(lib.setup).parameters) > 1:
            lib.setup(self, settings["cogs"][ext])
        else:
            lib.setup(self)

        self.extensions[name] = lib

    async def send_error(self, content, err):
        # cuz im lazy
        await self.send_status(f"- Error occured:\n{content}")
        await self.send_status(f"- {err}\n%s" % traceback.format_exc())

    async def send_status(self, msg):
        channel = self.get_channel(settings["core"].bot_log_channel)
        await channel.send(f"```diff\n{msg}```")

    async def on_command_error(self, context, exception):
        if hasattr(context.command, 'on_error') or \
           hasattr(context.cog, f"_{context.cog.__class__.__name__}__error"):
            return

    # Ready Event
    async def on_ready(self):
        # load config from database
        for server in self.guilds:
            self.servopts[server.id] = Config(self.db, server.id)

        await self.send_status("+ Logged in as:\n+ {0.name}\n+ {0.id}".format(self.user))
        self.ready = True


# load modules
if __name__ == '__main__':
    bot = Bot()
    folder = 'cogs'
    for extension in [
        f.replace('.py', '')
        for f in listdir(folder)
        if isfile(join(folder, f)) and f.split(".")[-1] == "py"
    ]:
        try:
            bot.load_extension(folder + "." + extension)
        except (discord.ClientException, ModuleNotFoundError) as e:
            print(f'Failed to load extension file \'{extension}.py\'')
            print(e)

    bot.run(settings["core"].bot_token)
