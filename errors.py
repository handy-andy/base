from discord.ext.commands.errors import CommandError


class ChanRestricted(CommandError):
    pass


class ModuleDisabled(CommandError):
    pass


class NotOwner(CommandError):
    pass


class PCPPError(Exception):
    def __init__(self, error):
        super().__init__()
        self.code = error


