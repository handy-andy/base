from discord.ext.commands import HelpFormatter, Paginator, Command
import itertools, asyncio, inspect


class Formatter(HelpFormatter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cmdList = 0

    async def _add_subcommands_to_page(self, max_width, commands):
        for name, command in commands:
            can_run = await self.context.bot.can_run(self.context, call_once=True)
            if name in command.aliases or \
               command.name in self.context.bot.servopts[self.context.guild.id].cmds_disabled or \
               not can_run:
                # skip aliases
                continue

            self.cmdList += 1

            entry = '  {0:<{width}} {1}'.format(name, command.short_doc, width=max_width)
            shortened = self.shorten(entry)
            self._paginator.add_line(shortened)

    @asyncio.coroutine
    def format(self):
        """Handles the actual behaviour involved with formatting.

        To change the behaviour, this method should be overridden.

        Returns
        --------
        list
            A paginated output of the help command.
        """
        self._paginator = Paginator()

        # we need a padding of ~80 or so

        description = self.command.description if not self.is_cog() else inspect.getdoc(self.command)

        if description:
            # <description> portion
            self._paginator.add_line(description, empty=True)

        if isinstance(self.command, Command):
            if self.command.brief:
                self._paginator.add_line(self.command.brief, empty=True)

            # <long doc> section
            if self.command.help:
                self._paginator.add_line(self.command.help.format(prefix=self.clean_prefix), empty=True)

            # end it here if it's just a regular command
            if not self.has_subcommands():
                self._paginator.close_page()
                return self._paginator.pages

        max_width = self.max_name_size

        def category(cb):
            return lambda tup: cb(tup[1])

        if self.context.channel.id in self.context.bot.servopts[self.context.guild.id].channels_restricted:
            self._paginator.add_line("Since this channel is restricted, the non-moderator commands are not shown.")
            self._paginator.add_line()
        filtered = yield from self.filter_command_list()

        self.cmdList = 0
        data = sorted(filtered, key=category(lambda c: c.instance.position))
        for category, commands in itertools.groupby(
                data, key=category(lambda c: [
                    getattr(c.instance, "name", c.cog_name) or '\u200bGeneral', c.instance.__class__.__name__.lower()
                ])
        ):
            # there simply is no prettier way of doing this.
            if category[1] in self.context.bot.servopts[self.context.guild.id].cogs_disabled:
                continue

            commands = sorted(commands)
            if len(commands) > 0:
                self._paginator.add_line(category[0]+":")

            yield from self._add_subcommands_to_page(max_width, commands)

        # add the ending note
        self._paginator.add_line()
        ending_note = self.get_ending_note()
        self._paginator.add_line(ending_note)
        return self._paginator.pages
